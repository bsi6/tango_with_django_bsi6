#coding: utf-8
from datetime import datetime
from django.template import RequestContext
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from rango.forms import CategoryForm, PageForm, UserForm, UserProfileForm
from rango.models import Category, Page, User, UserProfile

def decode_url(category_name_url):
    return category_name_url.replace('_', ' ')

def encode_url(category):
    return category.name.replace(' ', '_')

def get_category_list():
    cat_list = Category.objects.order_by('-likes')

    for cat in cat_list:
        cat.url = encode_url(cat)

    return cat_list

def index(request):
    context = {}

    cat_list = get_category_list()
    context['cat_list'] = cat_list

    category_list = Category.objects.order_by('-likes')[:5]
    context['categories'] = category_list
    for category in category_list:
        category.url = encode_url(category)

    page_list = Page.objects.order_by('-views')[:5]
    context['pages'] = page_list

    if request.session.get('last_visit'):
        last_visit_time = request.session.get('last_visit')
        visits = request.session.get('visits', 0)

        if (datetime.now() - datetime.strptime(last_visit_time[:-7], "%Y-%m-%d %H:%M:%S")).seconds > 5:
            request.session['visits'] = visits + 1
            request.session['last_visit'] = str(datetime.now())
    else:
        request.session['last_visit'] = str(datetime.now())
        request.session['visits'] = 1

    #print(request.session['visits'], request.session['last_visit'])

    return render(request, 'rango/index.html', context)

def about(request):
    context = {}
    cat_list = get_category_list()
    context['cat_list'] = cat_list

    context['mensagem_negrito'] = "Está página explica quem é o Rango."

    if request.session.get('visits'):
        visits = request.session.get('visits')
        last_visit = request.session.get('last_visit')
    else:
        visits = 0

    context['visits'] = visits
    context['last_visit'] = last_visit

    return render(request, 'rango/about.html', context)

def category(request, category_name_url):
    context = {}
    cat_list = get_category_list()
    context['cat_list'] = cat_list

    category_name = decode_url(category_name_url)

    context['category_name'] = category_name
    context['category_name_url'] = category_name_url
    try:
        category = Category.objects.get(name=category_name)
        context['category'] = category
        page_list = Page.objects.filter(category=category)
        context['pages'] = page_list
    except Category.DoesNotExist:
        pass
    return render(request, 'rango/category.html', context)
#    return HttpResponse("%s - %s - %s" %(category_name_url, category_name, category))

@login_required
def add_category(request):
    context = {}
    cat_list = get_category_list()
    context['cat_list'] = cat_list

    if request.method != 'POST':
        form = CategoryForm()
    else:
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            print form.errors

    context['form'] = form

    return render(request, 'rango/add_category.html', context)

@login_required
def add_page(request, category_name_url):
    context = {}
    cat_list = get_category_list()
    context['cat_list'] = cat_list

    context['category_name_url'] = category_name_url

    category_name = decode_url(category_name_url)
    context['category_name'] = category_name

    if request.method == 'POST':
        form = PageForm(request.POST)

        if form.is_valid():
            page = form.save(commit=False)
            try:
                cat = Category.objects.get(name=category_name)
                page.category = cat
            except Category.DoesNotExist:
                return render(request, 'rango/add_page.html', context)
            page.views = 0
            page.save()
            return category(request, category_name_url)
        else:
            print form.errors
    else:
        form = PageForm()

    context['form'] = form

    return render(request, 'rango/add_page.html', context)

def register(request):
    context = {}
    cat_list = get_category_list()
    context['cat_list'] = cat_list

    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            # print('Senha antes: %s' %user.password)
            user.set_password(user.password)
            user.save()
            # print('Senha depois: %s' %user.password)

            profile = profile_form.save(commit=False)
            profile.user = user

            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()

            registered = True
        else:
            print user_form.errors, profile_form.errors
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    context['user_form'] = user_form
    context['profile_form'] = profile_form
    context['registered'] = registered

    return render(request,
            'rango/register.html',
            context)

def user_login(request):
    context = {}
    cat_list = get_category_list()
    context['cat_list'] = cat_list

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/rango/')
            else:
                return HttpResponse("Sua conta no Rango está desabilitada.")
        else:
            msg_erro = "Detalhes de login inválidos: {0}, {1}".format(username, password)
            print(msg_erro)
            context['login_invalido'] = True
            return render(request, 'rango/login.html', context)
    else:
        return render(request, 'rango/login.html', context)

@login_required
def restricted(request):
    context = {}
    cat_list = get_category_list()
    context['cat_list'] = cat_list
    context['texto'] = "Como você fez o login, você pode ler esse texto!"
    return render(request, 'rango/restricted.html', context)

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/rango/')

@login_required
def profile(request):
    context = {}
    cat_list = get_category_list()
    context = {'cat_list': cat_list}

    u = User.objects.get(username=request.user)

    try:
        up = UserProfile.objects.get(user=u)
    except:
        up = None

    context['user'] = u
    context['userprofile'] = up
    return render(request, 'rango/profile.html', context)

def track_url(request):
    context = RequestContext(request)
    page_id = None
    url = '/rango/'
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                page = Page.objects.get(id=page_id)
                page.views = page.views + 1
                page.save()
                url = page.url
            except:
                pass

    return redirect(url)