#coding: utf-8
from django import forms
from rango.models import Category, Page, UserProfile
from django.contrib.auth.models import User

class CategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=128, help_text='Informe o nome da Categoria')
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    likes = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    class Meta:
        model = Category


class PageForm(forms.ModelForm):
    title = forms.CharField(max_length=128, help_text="Informe o título da página")
    url = forms.URLField(max_length=200, help_text="Informe a URL da página")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    class Meta:
        model = Page
        fields = ('title', 'url', 'views')


class UserForm(forms.ModelForm):
    username = forms.CharField(help_text="Informe o nome do usuário")
    email = forms.CharField(help_text="Informe o seu email")
    password = forms.CharField(widget=forms.PasswordInput(), help_text="Informe uma senha")

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

class UserProfileForm(forms.ModelForm):
    website = forms.URLField(help_text="Informe seu website", required=False)
    picture = forms.ImageField(help_text="Selecione uma imagem de perfil para upload", required=False)

    class Meta:
        model = UserProfile
        fields = ['website', 'picture']
